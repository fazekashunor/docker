const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const express = require('express');
let app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const data = require('./src/controller/dataC.js');

app.use(express.json()); // to support JSON-encoded bodies

mongoose.connect('mongodb://mongo:27017/Data', {
  useNewUrlParser: true,
}).then(() => {
  console.log('Successfully connected to the database');
}).catch((err) => {
  console.log('Could not connect to the database. Exiting now...', err);
  process.exit();
});

app.get('/', data.find);
app.delete('/', data.deleteById);
app.patch('/', data.patch);

app.listen(8080, () => {
    console.log('Server is listening on port 8080');
  });