const mongoose = require('mongoose');

const DataSchema = mongoose.Schema({
  AString: String,
  ANumber: Number,
});

module.exports = mongoose.model('Data', DataSchema, 'Datas');