const Data = require('../model/data.js');

function findAll(req, res){
    Data.find().then((datas) => {
        res.send(datas);
    }).catch(() => {
        res.status(500).send("Something Went Really Wrong!");
    });
};

function findbyId(req, res){
    const { id } = req.query;
    Data.findOne( {_id: id} ).then((data) => {
        res.send(data);
    }).catch(() => {
        res.status(500).send("Something Went Really Wrong!");
    });
};

exports.find = (req, res) => {
        const { id } = req.query;
        if( id ){
            findbyId(req, res);
        }else {
            findAll(req, res);
        }
};

function update(req, res){
    const { id } = req.query;
    const { AString } = req.query;
    const { ANumber } = req.query;
    Data.updateOne({ _id: id }, { $set: { AString: AString, ANumber: ANumber } } ).then((data)=>{
        if( !data ){
            res.send("Can't Update!");
        }
        else{
            res.send("updated!");
        }
    }).catch(()=>{
        res.status(500).send("Something Went Really Wrong!");
    })
};

 function create(req, res){
    const { AString } = req.query;
    const { ANumber } = req.query;
    const val = new Data({
        AString: AString,
        ANumber: ANumber,
        });
    val.save().then(()=>{
        res.send("Created!");
        })
    .catch(()=>{
        res.status(500).send("Something Went Really Wrong!");
    });
}

exports.patch = (req, res) => {
    const { id }  = req.query;
    const { AString } = req.query;
    const { ANumber } = req.query;
    if( id && AString && ANumber){
        update(req,res);
    }else if(AString && ANumber){
        create(req,res);
    }else{
        res.send("Unsupported Params!");
    }
}

exports.deleteById = (req, res) => {
    const { id } = req.query;
    Data.deleteOne( { _id: id } ).then(()=>{
        res.send("deleted!");
    }).catch(()=>{
        res.status(500).send("Something Went Really Wrong!");
    })
};